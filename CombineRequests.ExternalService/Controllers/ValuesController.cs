﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CombineRequests.ExternalService.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private static Dictionary<string, string[]> values = new Dictionary<string, string[]>
        {
            { CreateKey("user1", "client1"), new [] { "110", "111", "112" } },
            { CreateKey("user2", "client1"), new [] { "210", "211" } },
            { CreateKey("user1", "client2"), new [] { "120", "121", "122", "123" } },
            { CreateKey("user2", "client2"), new [] { "220", "221", "222", "223", "224" } }
        };

        private readonly ILogger<ValuesController> logger;

        public ValuesController(ILogger<ValuesController> logger)
        {
            this.logger = logger;
        }

        [HttpGet]
        public IEnumerable<string> Get(string userName, string clientName)
        {
            logger.LogWarning(new EventId(), $"Call - UserName: {userName}, ClientName: {clientName}");
            if (values.TryGetValue(CreateKey(userName, clientName), out var result))
            {
                return result;
            }
            return Enumerable.Empty<string>();
        }

        private static string CreateKey(string userName, string clientName)
        {
            return $"{userName}_{clientName}";
        }
    }
}
