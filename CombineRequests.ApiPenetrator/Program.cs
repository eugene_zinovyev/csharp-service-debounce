﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;

namespace CombineRequests.ApiPenetrator
{
    class Program
    {
        private static readonly HttpClient httpClient = new HttpClient();

        public static void Main(string[] args)
        {
            Task.WhenAll(new [] {
                PenetrateApi("user1", "client1", 6),
                PenetrateApi("user1", "client2", 4),
                // PenetrateApi("user2", "client1", 2),
                // PenetrateApi("user2", "client2", 1)
                })
                .GetAwaiter().GetResult();
        }

        private static Task PenetrateApi(string userName, string clientName, int times)
        {
            return Task.WhenAll(Enumerable.Range(0, times).Select(i => CallApi(userName, clientName)).ToList());
        }

        private static async Task CallApi(string userName, string clientName)
        {
            Console.WriteLine($"Calling Api - UserName: {userName}, ClientName: {clientName}");
            var response = await httpClient.GetStringAsync($"http://localhost:5000/api/values?userName={userName}&clientName={clientName}");
            Console.WriteLine(response);
        }
    }
}
