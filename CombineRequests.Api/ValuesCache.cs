using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace CombineRequests.Api
{
    public class ValuesCache : IValuesCache
    {
        private readonly MemoryCache memoryCache;

        public ValuesCache()
        {
            var cacheOptions = new MemoryCacheOptions();
            memoryCache = new MemoryCache(cacheOptions);
        }

        public Task<IReadOnlyCollection<string>> Get(ValuesCacheKey key)
        {
            return memoryCache.Get(key.ToString()) as Task<IReadOnlyCollection<string>>;
        }

        public void Set(ValuesCacheKey key, Task<IReadOnlyCollection<string>> value)
        {
            var options = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromMinutes(1));
            memoryCache.Set(key.ToString(), value, options);
        }
    }
}