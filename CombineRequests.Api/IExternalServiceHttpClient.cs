using System.Collections.Generic;
using System.Threading.Tasks;

namespace CombineRequests.Api
{
    public interface IExternalServiceHttpClient
    {
        Task<IReadOnlyCollection<string>> Get(string userName, string clientName);
    }
}