using System.Collections.Generic;
using System.Threading.Tasks;

namespace CombineRequests.Api
{
    public class ValuesProvider : IValuesProvider
    {
        private static readonly object lockObject = new object();

        private readonly IValuesCache valuesCache;
        private readonly IExternalServiceHttpClient externalServiceHttpClient;

        public ValuesProvider(IValuesCache valuesCache, IExternalServiceHttpClient externalServiceHttpClient)
        {
            this.valuesCache = valuesCache;
            this.externalServiceHttpClient = externalServiceHttpClient;
        }

        public Task<IReadOnlyCollection<string>> Get(string userName, string clientName)
        {
            lock (lockObject)
            {
                var key = new ValuesCacheKey(userName, clientName);
                var values = valuesCache.Get(key);
                if (values == null)
                {
                    values = externalServiceHttpClient.Get(userName, clientName);
                    valuesCache.Set(key, values);
                }
                return values;
            }
        }
    }
}