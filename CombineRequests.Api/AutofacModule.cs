using Autofac;

namespace CombineRequests.Api
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ValuesCache>().As<IValuesCache>().SingleInstance();
            builder.RegisterType<ExternalServiceHttpClient>().As<IExternalServiceHttpClient>().SingleInstance();
            builder.RegisterType<ValuesProvider>().As<IValuesProvider>().InstancePerDependency();
        }
    } 
}