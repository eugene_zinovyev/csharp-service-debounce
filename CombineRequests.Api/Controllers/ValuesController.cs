﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CombineRequests.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IValuesProvider valuesProvider;
        private readonly ILogger<ValuesController> logger;

        public ValuesController(IValuesProvider valuesProvider, ILogger<ValuesController> logger)
        {
            this.valuesProvider = valuesProvider;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<string>> Get(string userName, string clientName)
        {
            logger.LogWarning(new EventId(), $"Call - UserName: {userName}, ClientName: {clientName}");
            return await valuesProvider.Get(userName, clientName);
        }
    }
}
