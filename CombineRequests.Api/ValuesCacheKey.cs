namespace CombineRequests.Api
{
    public class ValuesCacheKey
    {
        public string UserName { get; }

        public string ClientName { get; }

        public ValuesCacheKey(string userName, string clientName)
        {
            UserName = userName;
            ClientName = clientName;
        }

        public override string ToString()
        {
            return $"{UserName}_{ClientName}";
        }
    }
}