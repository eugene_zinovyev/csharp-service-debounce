using System.Collections.Generic;
using System.Threading.Tasks;

namespace CombineRequests.Api
{
    public interface IValuesCache
    {
        Task<IReadOnlyCollection<string>> Get(ValuesCacheKey key);

        void Set(ValuesCacheKey key, Task<IReadOnlyCollection<string>> value);
    }
}