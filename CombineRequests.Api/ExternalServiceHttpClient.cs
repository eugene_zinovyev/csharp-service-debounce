using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CombineRequests.Api
{
    public class ExternalServiceHttpClient : IExternalServiceHttpClient
    {
        public async Task<IReadOnlyCollection<string>> Get(string userName, string clientName)
        {
            using(var httpClient = new HttpClient())
            {
                var responseString = await httpClient.GetStringAsync($"http://localhost:5002/api/values?userName={userName}&clientName={clientName}");
                return JsonConvert.DeserializeObject<IReadOnlyCollection<string>>(responseString);
            }
        }
    }
}